# Youload
A simple youtube playlist downloader

# How to use
1. Paste youtube playlist url in the url bar. Make sure its the url for the playlist and not one of the videos on the list. My favorite way to get the playlist url is by using the url you get when you click the share button
2. Choose the download type. You can choose one of the many resolution options or audio only.
3. Choose the folder to download into. By default its the download folder but you can use others. On android the music folder seems to not work right now.
4. Press download and wait (:

# Hacking away the source code
You will need pytube and kivy.

To run you can use "python3 src/main.py" and to build you will need buildozer.

While it has a model and view the project doesn't have a controller because of the way kivy is used.

# Current issues and things to add.
- Android doesn't like this app using the music folder.
- Doesn't have very user friendly sd card support.
- No iOS support
