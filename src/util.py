import os
import ffmpeg
import subprocess
from urllib.request import urlretrieve
from kivy.utils import platform
from kivy.app import App

def get_default_download_dir() -> str:
    if platform == "android":
        return "/sdcard/Download"
    else:
        from pathlib import Path
        return os.path.join(str(Path.home()), "Downloads")

def convert_mp4_to_mp3(input_file: str, output_file: str) -> None:
    ffmpeg.input(input_file).output(output_file).run()
