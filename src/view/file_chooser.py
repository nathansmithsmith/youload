from kivy.uix.popup import Popup
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.filechooser import FileChooserListView
from kivy.uix.textinput import TextInput

import util
import os

class YouloadFileChooser(Popup):

    def __init__(self, **kwargs):
        super(YouloadFileChooser, self).__init__(**kwargs)

        layout = BoxLayout(orientation='vertical')

        self.title = "Find folder"

        # Path input.
        self.path_input = TextInput(text=util.get_default_download_dir(), multiline=False, size_hint=(0.9, 1.0))
        self.path_input.bind(text=self.uid_path_input)

        # Close button.
        close_button = Button(text="Close", size_hint=(0.1, 1.0))
        close_button.bind(on_press=self.close_button_cb)

        top_bar_layout = BoxLayout(size_hint=(1.0, 0.1))
        top_bar_layout.add_widget(self.path_input)
        top_bar_layout.add_widget(close_button)

        # File chooser.
        self.file_chooser = FileChooserListView(size_hint=(1.0, 0.9))
        self.file_chooser.path = util.get_default_download_dir()

        layout.add_widget(top_bar_layout)
        layout.add_widget(self.file_chooser)
        self.content = layout

    def set_app(self, app):
        self.app = app

    def close_button_cb(self, instance):
        self.app.folder_display.text = self.get_folder()
        self.dismiss()

    def uid_path_input(self, instance, value):
        if os.path.exists(value):
            self.file_chooser.path = value

    def get_folder(self):
        return self.file_chooser.path

