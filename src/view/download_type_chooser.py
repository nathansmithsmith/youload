from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.popup import Popup
from kivy.uix.boxlayout import BoxLayout

from model.youload_playlist import YouLoadPlayList

class YouloadDownloadTypeChooser(Popup):

    def __init__(self, **kwargs):
        super(YouloadDownloadTypeChooser, self).__init__(**kwargs)

        self.title = "Download types"

        layout = BoxLayout(orientation="vertical")
        self.current_type = YouLoadPlayList.DEFAULT_DOWNLOAD_TYPE

        # Add option buttons.
        for download_type in YouLoadPlayList.DOWNLOAD_TYPES:
            item = Button(text=download_type)
            item.bind(on_press=self.option_button_cb)
            layout.add_widget(item)

        self.content = layout

    def set_app(self, app):
        self.app = app

    def option_button_cb(self, instance):
        self.current_type = instance.text
        self.app.download_type_button.text = self.current_type
        self.dismiss()

